from __future__ import division, print_function

def empirical_cdf(samples):
    import numpy

    def cdf(x):
        N = len(x)
        C = numpy.empty_like(x)

        for i in range(N):
            n = numpy.count_nonzero(x[i] >= samples)
            C[i] = n/N

        return C

    return cdf


def empirical_pdf(samples):
    import numpy

    cdf = empirical_cdf(samples)

    def pdf(x):
        N = len(x)
        C = cdf(x)

        dp = numpy.diff(C)
        dx = numpy.diff(x)

        return dp / dx

    return pdf
