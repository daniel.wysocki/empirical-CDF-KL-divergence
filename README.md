# empirical CDF KL divergence

A simple KL divergence calculator for two sets of 1D samples using an empirical CDF.


## Details

Approximates the KL divergence between two random variables, $`P`$ and $`Q`$:

```math
\mathrm{KL}(P || Q) = \int p(x) \ln\left[\frac{p(x)}{q(x)}\right] \mathrm{d}x,
```

given samples $`\{ p_i \}_{i=1}^N`$ and $`\{ q_i \}_{i=1}^M`$.

Approximates $`p(x)`$ and $`q(x)`$ from the samples by taking an empirical cumulative distribution function:

```math
\mathrm{eCDF}(x) = \frac{1}{N} \sum_{i=1}^N \Theta(x - x_i),
```

where $`\Theta(x)`$ is the Heaviside step function:

```math
\Theta(x) = \begin{cases} 1, & \mathrm{if} x \geq 0 \\ 0, & \mathrm{otherwise} \end{cases}
```

Once the CDF is approximated, we evaluate it on a fixed grid -- the same for both $`P`$ and $`Q`$ -- interpolate it linearly, and differentiate that linear interpolant, giving us the PDFs $`p(x)`$ and $`q(x)`$. If the grid-spacing is $`\Delta{x}`$, then the KL divergence can be computed _analytically_ by

```math
\mathrm{KL}(P || Q) = \sum_{b=1}^{N_{\mathrm{bins}}} p(x_b) \ln\left[\frac{p(x_b)}{q(x_b)}\right] \Delta{x}.
```






## Credit

Daniel Wysocki

Free to reuse, modify, and share under the conditions of the MIT License.
