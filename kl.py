from __future__ import division, print_function

def KL(p_samples, q_samples, x_min=None, x_max=None, n_interp=100):
    import numpy
    import utils

    p = utils.empirical_pdf(p_samples)
    q = utils.empirical_pdf(q_samples)

    if x_min is None:
        x_min = min(numpy.min(p_samples), numpy.min(q_samples))
    if x_max is None:
        x_max = max(numpy.max(p_samples), numpy.max(q_samples))

    x, dx = numpy.linspace(x_min, x_max, n_interp, retstep=True)

    px, qx = p(x), q(x)
    px_nonzero = px > 0
    px = px[px_nonzero]
    qx = qx[px_nonzero]

    return dx * numpy.sum(px * numpy.log(px / qx))
