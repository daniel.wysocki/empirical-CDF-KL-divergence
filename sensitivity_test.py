from __future__ import division, print_function

import numpy
import kl

seed = 4

random = numpy.random.RandomState(seed)

ngridpoints = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]

unif_samples = random.uniform(size=10000)
norm_samples = random.normal(size=10000)


numpy.warnings.filterwarnings("ignore")

print("Each uses the following number of evenly-spaced CDF grid points, respectively")
print(ngridpoints)

print(
    "KL(U(0,1), U(0,1)) =",
    [
        kl.KL(unif_samples, unif_samples, x_min=0, x_max=1, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(U(0,1), U(0,1)) | 90% of samples =",
    [
        kl.KL(unif_samples, unif_samples[:9000], x_min=0, x_max=1, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(U(0,1), U(0,1)) | two halves =",
    [
        kl.KL(unif_samples[:5000], unif_samples[5000:], x_min=0, x_max=1, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)


print(
    "KL(N(0,1), N(0,1)) =",
    [
        kl.KL(norm_samples, norm_samples, x_min=-5, x_max=+5, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(N(0,1), N(0,0.99)) =",
    [
        kl.KL(norm_samples, 0.99*norm_samples, x_min=-5, x_max=+5, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(N(0,1), N(0,1.01)) =",
    [
        kl.KL(norm_samples, 1.01*norm_samples, x_min=-5, x_max=+5, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(N(0,0.99), N(0,1)) =",
    [
        kl.KL(0.99*norm_samples, norm_samples, x_min=-5, x_max=+5, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)

print(
    "KL(N(0,1.01), N(0,1)) =",
    [
        kl.KL(1.01*norm_samples, norm_samples, x_min=-5, x_max=+5, n_interp=ngrid)
        for ngrid in ngridpoints
    ]
)
